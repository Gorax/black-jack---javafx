package black_jack;

public class Deck {

    private Card[] cards = new Card[208];

    public Deck() {
        refill();
    }

    public final void refill() {
        int count = 0;
        for (int i = 0; i < 4; i++) {
            for (Card.Suit suit : Card.Suit.values()) {
                for (Card.Rank rank : Card.Rank.values()) {
                    cards[count++] = new Card(suit, rank);
                }
            }
        }
    }

    public Card drawCard() {
        Card card = null;
        while (card == null) {
            int index = (int) (Math.random() * cards.length);
            card = cards[index];
            cards[index] = null;
        }
        return card;
    }
}
