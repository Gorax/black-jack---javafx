package black_jack;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.HBox;

public class Controller {

    private Deck deck;
    private Hand croupier, player, playerSplit;
    private BasicStrategy basicStrategy;
    private int betAmount;
    private long money = 1000;
    private int withoutW = 0;
    private int withoutL = 0;
    private int withW = 0;
    private int withL = 0;

    @FXML
    private Label croupierPoint;

    @FXML
    private Label bet;

    @FXML
    private Label playerPoint;

    @FXML
    private Label playerSplitPoint;

    @FXML
    private Label playerMoney;

    @FXML
    private Label withoutWin;

    @FXML
    private Label withoutLose;

    @FXML
    private Label withWin;

    @FXML
    private Label withLose;

    @FXML
    private HBox dealerCards;

    @FXML
    private HBox playerCards;

    @FXML
    private HBox playerCardsSplit;

    @FXML
    private RadioButton radioButton;

    public void initialize() {
        deck = new Deck();
        croupier = new Hand(dealerCards.getChildren());
        player = new Hand(playerCards.getChildren());
        playerSplit = new Hand(playerCardsSplit.getChildren());

        basicStrategy = new BasicStrategy();

        playerMoney.setText("" + money);
        bet.setText("" + betAmount);
        withoutWin.setText("" + withoutW);
        withoutLose.setText("" + withoutL);
        withWin.setText("" + withW);
        withLose.setText("" + withL);

        croupierPoint.textProperty().bind(croupier.valueProperty().asString());
        playerPoint.textProperty().bind(player.valueProperty().asString());
        playerSplitPoint.textProperty().bind(playerSplit.valueProperty().asString());
    }

    @FXML
    private void startGame() {
        start();
        setBet();
        setStatistic();
        if (radioButton.isSelected()) {
            yoloPlayer();
        } else {
            playerWithBasicStrategy();
        }
    }

    private void start() {
        deck.refill();

        croupier.reset();
        player.reset();
        playerSplit.reset();

        croupier.takeCard(deck.drawCard());
        croupier.takeCard(deck.drawCard());

        player.takeCard(deck.drawCard());
        player.takeCard(deck.drawCard());

        checkPlayer();
    }

    private void yoloPlayer() {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                try {
                    if (betAmount > 0) {
                        if (!playerSplit.getCards().isEmpty()) {
                            split(15);
                        } else {
                            while (true) {
                                if (player.valueProperty().get() <= 15) {
                                    playerHit();
                                    if (checkForPlayerLose()) {
                                        withoutL++;
                                        break;
                                    }
                                } else if (croupier.valueProperty().get() < 17) {
                                    croupierHit();
                                    if (checkForCroupierLose()) {
                                        money += betAmount * 2;
                                        withoutW++;
                                        break;
                                    }
                                } else {
                                    checkForWin();
                                    break;
                                }
                            }
                        }
                        startNextRound();
                    } else {
                        System.out.println("player lost all money");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    private void playerWithBasicStrategy() {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                try {
                    if (betAmount > 0) {
                        char move = basicStrategy.checkCards(player.getCards(), croupier.getCards(), playerSplit.getCards(), player.valueProperty().get());

                        if ('h' == move) {
                            playerHit();
                        } else if ('s' == move) {
                            stand();
                        } else if ('d' == move) {
                            doubleDown();
                        }

                        if (('p' == move)) {
                            split(16);
                        } else if (!checkForPlayerLose()) {
                            checkIfNotSplit();
                        }
                        startNextRound();
                    } else {
                        System.out.println("player lost all money");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    private void setBet() {
        betAmount = 10;
        if (money > 0) {
            money -= betAmount;
        } else {
            betAmount = 0;
        }
        bet.setText("" + betAmount);
        playerMoney.setText("" + money);
    }

    private boolean checkForPlayerLose() {
        if (player.valueProperty().get() > 21) {
            System.out.println("croupier win!");
            return true;
        }
        return false;
    }

    private boolean checkForCroupierLose() {
        if (croupier.valueProperty().get() > 21) {
            System.out.println("player win!");
            return true;
        }
        return false;
    }

    private void checkForWin() {
        if (player.valueProperty().get() > 21) {
            System.out.println("Croupier win!");
            if (radioButton.isSelected()) {
                withoutL++;
            } else {
                withL++;
            }
        } else if (croupier.valueProperty().get() > 21) {
            System.out.println("Player win!");
            money += betAmount * 2;
            if (radioButton.isSelected()) {
                withoutW++;
            } else {
                withW++;
            }
        } else if (player.valueProperty().get() > croupier.valueProperty().get()) {
            System.out.println("player win!");
            money += betAmount * 2;
            if (radioButton.isSelected()) {
                withoutW++;
            } else {
                withW++;
            }
        } else if (croupier.valueProperty().get() > player.valueProperty().get()) {
            System.out.println("croupier win!");
            if (radioButton.isSelected()) {
                withoutL++;
            } else {
                withL++;
            }
        } else {
            System.out.println("draw");
            money += betAmount;
        }
    }

    private void split(int limit) throws InterruptedException {
        while (true) {
            if (player.valueProperty().get() <= limit) {
                Thread.sleep(1500);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        player.takeCard(deck.drawCard());
                    }
                });
                Thread.sleep(1500);
                continue;
            } else if (playerSplit.valueProperty().get() <= limit) {
                Thread.sleep(1500);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        playerSplit.takeCard(deck.drawCard());
                    }
                });
                Thread.sleep(1500);
                continue;
            } else if (croupier.valueProperty().get() < 17) {
                Thread.sleep(1500);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        croupier.takeCard(deck.drawCard());
                    }
                });
                Thread.sleep(1500);
                continue;
            }

            if (player.valueProperty().get() > 21 && playerSplit.valueProperty().get() > 21) {
                System.out.println("croupier win!");
                if (radioButton.isSelected()) {
                    withoutL++;
                } else {
                    withL++;
                }
                break;
            } else if (croupier.valueProperty().get() > 21) {
                System.out.println("player win!");
                money += betAmount * 2;
                if (radioButton.isSelected()) {
                    withoutW++;
                } else {
                    withW++;
                }
                break;
            } else if (
                    player.valueProperty().get() <= 21 && player.valueProperty().get() > croupier.valueProperty().get() ||
                            playerSplit.valueProperty().get() <= 21 && playerSplit.valueProperty().get() > croupier.valueProperty().get()
            ) {
                System.out.println("player win!");
                money += betAmount * 2;
                if (radioButton.isSelected()) {
                    withoutW++;
                } else {
                    withW++;
                }
                break;
            } else if (
                    player.valueProperty().get() <= 21 && player.valueProperty().get() == croupier.valueProperty().get() ||
                            playerSplit.valueProperty().get() <= 21 && playerSplit.valueProperty().get() == croupier.valueProperty().get()
            ) {
                System.out.println("Draw!");
                money += betAmount;
                break;
            } else {
                System.out.println("Croupier win!");
                if (radioButton.isSelected()) {
                    withoutL++;
                } else {
                    withL++;
                }
                break;
            }
        }
    }

    private void stand() throws InterruptedException {
        System.out.println("Stand");
        Thread.sleep(2000);
    }

    private void playerHit() throws InterruptedException {
        System.out.println("player Hit");
        Thread.sleep(1500);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                player.takeCard(deck.drawCard());
            }
        });
        Thread.sleep(1500);
    }

    private void croupierHit() throws InterruptedException {
        Thread.sleep(1500);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                croupier.takeCard(deck.drawCard());
            }
        });
        Thread.sleep(1500);
    }

    private void doubleDown() throws InterruptedException {
        System.out.println("double");
        money -= betAmount;
        betAmount *= 2;
        Thread.sleep(1500);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                player.takeCard(deck.drawCard());
            }
        });
        Thread.sleep(1500);
    }

    private void startNextRound() throws InterruptedException {
        System.out.println("Next Round");
        System.out.println("----------------------");
        Thread.sleep(5000);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                startGame();
            }
        });
    }

    private void checkIfNotSplit() throws InterruptedException {
        while (true) {
            if (croupier.valueProperty().get() <= 16) {
                croupierHit();
            } else {
                if (checkForCroupierLose()) {
                    money += betAmount * 2;
                    withW++;
                } else {
                    checkForWin();
                }
                break;
            }
        }
    }

    private void splitCards() {
        Card check = (Card) player.getCards().get(0);
        if (check.value < 11) {
            playerSplit.getCards().add(player.getCards().get(1));
            int temp = player.valueProperty().get() / 2;
            player.valueProperty().set(temp);
            playerSplit.valueProperty().set(temp);
        } else if (check.value == 11) {
            playerSplit.getCards().add(player.getCards().get(1));
            playerSplit.setAces(1);
            playerSplit.valueProperty().set(11);
            player.valueProperty().set(11);
        }
    }

    private void setStatistic() {
        withoutLose.setText("" + withoutL);
        withoutWin.setText("" + withoutW);
        withWin.setText("" + withW);
        withLose.setText("" + withL);
    }

    private void checkPlayer() {
        if (radioButton.isSelected()) {
            Card card1 = (Card) player.getCards().get(0);
            Card card2 = (Card) player.getCards().get(1);
            if (card1.value == card2.value) {
                splitCards();
            }
        } else {
            char move = basicStrategy.checkCards(player.getCards(), croupier.getCards(), playerSplit.getCards(), player.valueProperty().get());
            if ('p' == move) {
                splitCards();
            }
        }
    }
}
