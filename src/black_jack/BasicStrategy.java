package black_jack;

import javafx.collections.ObservableList;
import javafx.scene.Node;

public class BasicStrategy {

    private char[][] point = {
            {'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h', 'h'},
            {'h', 'd', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'},
            {'d', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'h', 'h'},
            {'d', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'h'},
            {'h', 'h', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'},
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'},
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'},
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'},
            {'s', 's', 's', 's', 's', 'h', 'h', 'h', 'h', 'h'},
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}
    };

    private char[][] pairWithAce = {
            {'h', 'h', 'h', 'd', 'd', 'h', 'h', 'h', 'h', 'h'},
            {'h', 'h', 'h', 'd', 'd', 'h', 'h', 'h', 'h', 'h'},
            {'h', 'h', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'},
            {'h', 'h', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'},
            {'h', 'd', 'd', 'd', 'd', 'h', 'h', 'h', 'h', 'h'},
            {'s', 'd', 'd', 'd', 'd', 's', 's', 'h', 'h', 'h'},
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'},
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}
    };

    private char[][] pair = {
            {'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'h'},
            {'p', 'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'},
            {'p', 'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'},
            {'h', 'h', 'h', 'p', 'p', 'h', 'h', 'h', 'h', 'h'},
            {'d', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'h', 'h'},
            {'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h', 'h'},
            {'p', 'p', 'p', 'p', 'p', 'p', 'h', 'h', 'h', 'h'},
            {'p', 'p', 'p', 'p', 'p', 'p', 'p', 'p', 'h', 'h'},
            {'p', 'p', 'p', 'p', 'p', 's', 'p', 'p', 's', 's'},
            {'s', 's', 's', 's', 's', 's', 's', 's', 's', 's'}
    };

    public BasicStrategy() {
    }

    public char checkCards(ObservableList<Node> playerCards, ObservableList<Node> croupierCards, ObservableList<Node> playerCardsSplit, int playerPoints) {
        Card playerCard0 = (Card) playerCards.get(0);
        Card playerCard1;

        if (playerCards.size() > 1) {
            playerCard1 = (Card) playerCards.get(1);
        } else {
            playerCard1 = (Card) playerCardsSplit.get(0);
        }

        Card croupierCard0 = (Card) croupierCards.get(0);

        if (playerCard0.rank == playerCard1.rank) {
            return pair(playerCard0, croupierCard0);
        } else if (playerCard0.rank.value == 11 && playerCard1.rank.value <= 9) {
            return pairWithAce(playerCard1, croupierCard0);
        } else if (playerCard1.rank.value == 11 && playerCard0.rank.value <= 9) {
            return pairWithAce(playerCard0, croupierCard0);
        } else {
            return points(croupierCard0, playerPoints);
        }
    }

    private char points(Card croupierCard, int playerPoints) {
        int x = croupierCard.rank.value - 2;
        int y = 0;

        if (playerPoints >= 17) {
            y = 9;
        } else if (playerPoints >= 9) {
            y = playerPoints - 8;
        }

        return point[y][x];
    }

    private char pair(Card playerCard, Card croupierCard) {
        int x = croupierCard.rank.value - 2;
        int y = 0;

        if (playerCard.rank.value < 11) {
            y = playerCard.rank.value - 1;
        }

        return pair[y][x];
    }

    private char pairWithAce(Card playerCard, Card croupierCard) {
        int x = croupierCard.rank.value - 2;
        int y = playerCard.rank.value - 2;

        return pairWithAce[y][x];
    }
}
